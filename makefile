all:
	$(MAKE) clean
	$(MAKE) st
	./st
st:
	$(CC) -I/usr/X11R6/include -I/usr/include/freetype2 -D_XOPEN_SOURCE=600 -O3 st.c \
	      -o $@ -L/usr/X11R6/lib -lm -lrt -lX11 -lutil -lXft -lfontconfig -lfreetype \
	      -Wno-int-conversion -Wno-pointer-sign -Wno-sentinel -Wno-shift-op-parentheses -Wunused-function -Wunused-variable
clean:
	rm -f st

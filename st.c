// ngn/st - simplified https://st.suckless.org/
#define CLR "#000","#a00","#0a0","#a50","#00a","#a0a","#0aa","#aaa","#555","#f55","#5f5","#ff5","#55f","#f5f","#5ff","#fff",[256]="#ccc","#555","gray90","black"
//#define CLR "black","red3","green3","yellow3","blue2","magenta3","cyan3","gray90","gray50","red","green","yellow","#5c5cff","magenta","cyan","white",[256]="#cccccc","#555555","gray90","black"
#define FNT "Monospace:pixelsize=15:antialias=1:autohint=1"
#define HSZ 10000 //history size
#define MXW 256   //max width
#define MXH 256   //max height
#define VOL -100  //bell volume (-100..100)

#include<ctype.h>
#include<errno.h>
#include<pwd.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/select.h>
#include<sys/wait.h>
#include<unistd.h>
#include<wchar.h>
#if   defined(__linux)
 #include<pty.h>
#elif defined(__OpenBSD__)||defined(__NetBSD__)||defined(__APPLE__)
 #include<util.h>
#elif defined(__FreeBSD__)||defined(__DragonFly__)
 #include<libutil.h>
#endif

#define      _(a...) {return({a;});}
#define    P(x,a...) if(x)_(a)
#define    I(x,a...) if(x){a;}
#define    J(x,a...) else if(x){a;}
#define      E(a...) else{a;}
#define    W(x,a...) while(x){a;}
#define    C(x,a...) case x:{a;}break;
#define   C2(x,a...) case x:C(a)
#define   C3(x,a...) case x:C2(a)
#define   C4(x,a...) case x:C3(a)
#define   C5(x,a...) case x:C4(a)
#define      i(a...) F(i,0,a)
#define      j(a...) F(j,0,a)
#define    p(x,a...) i(LEN(x),V(p,(x)+i)a)
#define SW(x,y,a...) switch(x){default:{y;}break;a}
#define     MN(a...) MM(<,a)
#define     MX(a...) MM(>,a)
#define F(i,s,e,a...) for(I i=(s),e_=(e);i<e_;i++){a;}
#define MM(c,x,y) ({V(x_,x)V(y_,y)x_ c y_?x_:y_;})
#define V(x,y) TY(y)x=y;
#define TY __typeof__
#define O const
#define S static
#define R return
#define ST struct
#define TD typedef
#define SZ sizeof
#define BR break
TD void V,V0();TD char C;TD unsigned char UC;TD unsigned short UH;TD int I;TD unsigned int UI;TD size_t N;

//ERROR HANDLING
#define PE(a...) fprintf(stderr,a)
#define  Q(a...) ({V(r_,a)if(!r_) die("falsy:"   #a"\n",__LINE__,__FUNCTION__);r_;})//assert truthy
#define QN(a...) ({V(r_,a)if(r_<0)die("negative:"#a"\n",__LINE__,__FUNCTION__);r_;})//assert non-negative
#define QW(a...) ({V(r_,a)if(!r_)warn("falsy:"   #a"\n",__LINE__,__FUNCTION__);r_;})//warn if not truthy
S V warn(O C*s,I l,O C*f){PE("%s\nline:%d\nfunc:%s\n",s,l,f);I(errno,PE("strerror:%s\n",strerror(errno)))}S V die(O C*s,I l,O C*f){warn(s,l,f);_exit(1);}

#define LEN(x)    (SZ(x)/SZ((x)[0]))         //length
#define SWP(x,y)  {V(z_,x)(x)=(y);(y)=z_;}   //swap
#define DF(x,y)   (x)=(x)?(x):(y);           //default
#define LM(x,y)   (x)=MX(0,MN((x),(y)-1));   //limit
#define BT(x,s,b) ((s)?(x)|=(b):((x)&=~(b))) //set or unset bit
#define SCR       scr[tM(TA)]                //active screen
#define TL(y)     (SCR.l[TLO(y)])            //terminal line
#define TLO(y)    (((y)+SCR.cur-SCR.o+SCR.n)%SCR.n)//terminal line offset
#define TDF(x,y)  (((x).tv_sec-(y).tv_sec)*1000+((x).tv_nsec-(y).tv_nsec)/1E6)//time diff
//window modes:WV=visible,WF=focused,WAK=appkeypad,WMB=mousebutton,WMMo=mousemotion,WR=reverse,WK=keyboardlock,WH=hide,WAC=appcursor,WMG=mousegr,W8=8bit,WB=blink,WFB=fblink,Wf=focus,WX=mousex10,
// WMMa=mousemany,WBP=bracketpaste,WN=numlock
//glyph attributes:AB=bold,AF=faint,AI=italic,AU=underline,ABl=blink,AR=reverse,AIn=invisible,AS=strike,AWr=wrap,AWi=wide,AD=dummy
//escape flags:E0=start,EC=csi,ES=str,EA=altcharset,EE=str_end,ET=test,EU=utf8
//G:glyph{u=unicodepoint,a=attributes,f=foreground,b=background}  tm=terminal mode:TW=wrap,TI=insert,TA=altscreen,TC=crlf,TE=echo,TP=print,TU=utf8
//Cur:cursor{g:glyph,x:column,y:row,s:state(0=default,1=wrapnext,2=original)}  tran:translation table, lc:last char, ll:line length, tcs:terminal charset, tics:tcs index,
//scr:screens{line,size,cursor,offset,screencursor}
enum{WV=1,WF=2,WAK=4,WMB=8,WMMo=16,WR=32,WK=64,WH=128,WAC=256,WMG=512,W8=1024,WB=2048,WFB=4096,Wf=8192,WX=1<<14,WMMa=1<<15,WBP=1<<16,WN=1<<17,WMM=WMB|WMMo|WX|WMMa};
enum{AB=1,AF=2,AI=4,AU=8,ABl=16,ABl2=32,AR=64,AIn=128,AS=256,AWr=512,AWi=1024,AD=2048};S enum{E0=1,EC=2,ES=4,EA=8,EE=16,ET=32,EU=64}esc;S enum{TW=1,TI=2,TA=4,TC=8,TE=16,TP=32,TU=64}tm;
TD ST{UI u,a,f,b;}G;TD ST{G g;I x,y,s;}Cur;S C tran[4];S UI lc;S C tabs[MXW];I ll,tw,th,ty0,ty1,tcs,tics;S Cur tc;S ST{G*l[HSZ];I n,cur,o;Cur sc;}scr[2];
S V0 rdr,xbe,xcp,xd0,xlC,csiD,strp,tDs,tD;S V xdc(I,I,G,I,I,G),xdl(G*,I,I,I),xsi(C*),xst(C*),xm(I,UI),xpm(I),xxs(I,I),tDl(I),tcr(I,I,I,I),tmv(I,I),ttab(I),tpc(UI),tdirt(I,I);
S I twr(O UC*,I,I),xgc(I,UC*,UC*,UC*),xcn(I,O C*),xsc(I),xd1();

//UTILS  u8e,u8d:utf8 encode/decode, tM:test terminal mode bit, cc:control char?
S N u8e(UI u,C*s)_(P(u<128,*s=u;1)P(u<2048,*s=u>>6&31|192;s[1]=u&63|128;2)P(u<65536,*s=u>>12&15|224;s[1]=u>>6&63|128;s[2]=u&63|128;3)*s=u>>18&7|240;s[1]=u>>12&63|128;s[2]=u>>6&63|128;s[3]=u&63|128;4)
S N u8d(O UC*s,UI*u,N n)_(UC c=*s;I l=__builtin_clz(~c<<24);l^=!l;P(!l||4<l||n<l,*u=0xfffd;0)*u=l==1?c:l==2?c%32<<6|s[1]&63:l==3?c%16<<12|s[1]%64<<6|s[2]&63:c%8<<18|s[1]%64<<12|s[2]%64<<6|s[3]&63;l)
S I tM(UI x)_(!!(tm&x))S I cc1(UI x)_(x-128<32u)S I cc(UI x)_(x<32u||x==127||cc1(x))

//SELECTION  sm:mode(0=idle,1=empty,2=ready), st:type(1=regular,2=rectangle), snap(1=word,2=line), sa:alternate screen, ox,nY..:old start x,new end y,..
//selcl:clear, ly:line length, dlm:delimiter, selsn:snap, seln:normalize, sel:is selected?, gsel:get selection, selsc:scroll, selst:start, selx:expand
S I sm,st,snap,sa,nx,ny,nX,nY,ox=-1,oy,oX,oY;
S V selcl(){P(ox<0)sm=0;ox=-1;tdirt(ny,nY);}S I ly(I y)_(I i=tw;G*l=TL(y);P(l[i-1].a&AWr,i)W(i>0&&l[i-1].u==32,--i)i)S I dlm(UI x)_(x==32)
S V selsn(I*x,I*y,I d){I(snap==2,*x=d<0?0:tw-1;I(d<0,W(*y>0&&(TL(*y-1)[tw-1].a&AWr),*y+=d))J(d>0,W(*y<th-1&&(TL(*y)[tw-1].a&AWr),*y+=d)))
 J(snap==1,O G*pg=TL(*y)+*x;I pd=dlm(pg->u);W(1,I x1=*x+d,y1=*y;I(x1>=(UI)tw,y1+=d;x1=(x1+tw)%tw;I(y1>=(UI)th,BR)I xt=d>0?*x:x1,yt=d>0?*y:y1;I(!(TL(yt)[xt].a&AWr),BR))
                                                I(x1>=ly(y1),BR)O G*g=TL(y1)+x1;I q=dlm(g->u);I(!(g->a&AD)&&(q-pd||(q&&g->u-pg->u)),BR)*x=x1;*y=y1;pg=g;pd=q))}
S V seln(){I(st==1&&oy-oY,nx=oy<oY?ox:oX;nX=oy<oY?oX:ox)E(nx=MN(ox,oX);nX=MX(ox,oX))ny=MN(oy,oY);nY=MX(oy,oY);selsn(&nx,&ny,-1);selsn(&nX,&nY,+1);P(st==2)I i=ly(ny);I(i<nx,nx=i)I(ly(nY)<=nX,nX=tw-1)}
S I sel(I x,I y)_(sm==1||ox<0||sa-tM(TA)?0:st==2?ny<=y&&y<=nY&&nx<=x&&x<=nX:ny<=y&&y<=nY&&(y-ny||x>=nx)&&(y-nY||x<=nX))
S C*gsel()_(P(ox<0,0)I l;O G*g,*pg;C*s,*p=s=Q(malloc((tw+1)*(nY-ny+1)*4));
 F(y,ny,nY+1,I n=ly(y);I(!n,*p++=10;continue)I(st==2,g=TL(y)+nx;l=nX)E(g=TL(y)+(ny==y)*nx;l=nY-y?tw-1:nX)
  pg=TL(y)+MN(l,n-1);W(pg>=g&&pg->u==32,--pg)W(g<=pg,I(!(g->a&AD),p+=u8e(g->u,p))g++)I((y<nY||l>=n)&&(!(pg->a&AWr)||st==2),*p++=10))*p=0;s)
S V selsc(I o,I n){P(ox<0)I b=o<=ny&&ny<=ty1;I(b-(o<=nY&&nY<=ty1),selcl())J(b,oy+=n;oY+=n;oy<ty0||oy>ty1||oY<ty0||oY>ty1?selcl():seln())}
S V selst(I x,I y,I sn){selcl();sm=1;st=1;sa=tM(TA);snap=sn;oX=ox=x;oY=oy=y;seln();I(sn,sm=2)tdirt(ny,nY);}
S V selx(I x,I y,I t,I done){P(!sm)P(done&&sm==1,selcl();)I pY=oY,pX=oX,qy=ny,qY=nY,pt=st;oX=x;oY=y;seln();st=t;I(pY-oY||pX-oX||pt-st||sm==1,tdirt(MN(ny,qy),MX(nY,qY)))sm=done?0:2;}

//TTY  tnm:terminal name, iofd:i/o file descriptor, cfd:command file descriptor, ttyr:read, ttyw:write, ttyW:write raw, ttysz:resize, ttyhup:hang up
S C*tnm="st-256color";S pid_t pid;S I iofd=1,cfd;
S V execsh(C*cmd,C**a){errno=0;O ST passwd*pw=Q(getpwuid(getuid()));C*sh=getenv("SHELL");DF(sh,pw->pw_shell[0]?pw->pw_shell:cmd);C*p=a?*a:sh;a=a?a:(C*[3]){p};
 unsetenv("COLUMNS");unsetenv("LINES");unsetenv("TERMCAP");setenv("LOGNAME",pw->pw_name,1);setenv("USER",pw->pw_name,1);setenv("SHELL",sh,1);setenv("HOME",pw->pw_dir,1);setenv("TERM",tnm,1);
 i(6,signal((I[]){SIGCHLD,SIGHUP,SIGINT,SIGQUIT,SIGTERM,SIGALRM}[i],SIG_DFL))execvp(p,a);_exit(1);}
S V sigc(I x){I r;P(pid-QN(waitpid(pid,&r,WNOHANG)))Q(!WIFEXITED(r)||!WEXITSTATUS(r));Q(!WIFSIGNALED(r));_exit(0);}
S V ttynew(C*x){I s;QN(openpty(&cfd,&s,0,0,0));pid=QN(fork());I(pid,close(s);signal(SIGCHLD,sigc))E(close(iofd);close(cfd);setsid();i(3,dup2(s,i))QN(ioctl(s,TIOCSCTTY,0));I(s>2,close(s))execsh(x,0))}
S N ttyr()_(S C s[8192];S I n=0;I r=QN(read(cfd,s+n,SZ(s)-n));I(!r,exit(0))n+=r;I w=twr(s,n,0);n-=w;I(n>0,memmove(s,s+w,n))r)
S V ttyW(O C*s,N n){fd_set w,r;N m=256;W(n>0,FD_ZERO(&w);FD_ZERO(&r);FD_SET(cfd,&w);FD_SET(cfd,&r);Q(pselect(cfd+1,&r,&w,0,0,0)>=0||errno-EINTR);
                                             I(FD_ISSET(cfd,&w),I k=QN(write(cfd,s,MN(n,m)));I(k<n,I(n<m,m=ttyr())n-=k;s+=k)E(BR))I(FD_ISSET(cfd,&r),m=ttyr()))}
S V ttyw(O C*s,N n,I e){O C*p;I(e&&tM(TE),twr(s,n,1))P(!tM(TC),ttyW(s,n);)W(n>0,I(*s==13,p=s+1;ttyW("\r\n",2))E(p=memchr(s,13,n);p=p?p:s+n;ttyW(s,p-s))n-=p-s;s=p)}
S V ttysz(I w,I h){QN(ioctl(cfd,TIOCSWINSZ,&(ST winsize){.ws_row=th,.ws_col=tw,.ws_xpixel=w,.ws_ypixel=h}));}
S V ttyhup(){kill(pid,SIGHUP);}

//TERMINAL  dcs,df,db:default charset/foreground/background, CSUS:US charset, dty:dirty, tat:is attribute a used?, tcur:cursor(0=save,1=load), tscrl:scroll, cll:clear line, eL:ensure lines,
//tsz:resize, talt:alternate screen?, tswp:swap screens, tnl:new line, tmv:move, tsc:set char, tcr:clear region, tdc:delete char, tib:insert blank, til:insert line, tdl:delete line,
//tsa:set attribute, tsm:set mode
S O I dcs=256,df=258,db=259,CSUS=3;S C dty[MXH];
S V tid(){ttyw("\e[?6c",5,0);}
S I tat(I a)_(I y=TLO(0);i(th-1,G*l=SCR.l[y];j(tw-1,P(l[j].a&a,1))y=(y+1)%SCR.n)0)
S V tdirt(I y0,I y1){LM(y0,th)LM(y1,th)memset(dty+y0,1,y1-y0+1);}
S V tdirtat(I a){I y=TLO(0);i(th-1,G*l=SCR.l[y];j(tw-1,I(l[j].a&a,tdirt(i,i);BR))y=(y+1)%SCR.n)}
S V tDIRT(){tdirt(0,th-1);}
S V tcur(I x){P(x,tc=SCR.sc;tmv(tc.x,tc.y))SCR.sc=tc;}
S V tscrl(I y0,I y1){LM(y0,th)LM(y1,th)ty1=y0^y1^(ty0=MN(y0,y1));}
S V cll(G*l,G g,I x0,I x1){g.a=0;g.u=32;F(i,x0,x1,l[i]=g)}
S G*el(G**l)_(DF(*l,Q(malloc(MXW*SZ(G)))))
S V eL(G**l,I n){i(n,el(l+i))}
S V tsz(I w,I h){P(w<1||h<1||w>MXW||h>MXH,PE("error resizing to %dx%d\n",w,h);{})V(s,scr+0)V(t,scr+1)I(h<=tc.y,s->cur=(s->cur-h+tc.y+1)%s->n)
 I n=MX(w,ll);I(n>ll,i(s->n,I(s->l[i],cll(s->l[i],tc.g,ll,n)))i(MN(h,th),cll(t->l[i],tc.g,ll,n)))
 I j=s->cur;i(h,el(s->l+j);I(i>=th,cll(s->l[j],tc.g,0,n))j=(j+1)%s->n)t->cur=0;t->n=h;F(i,th,h,cll(el(t->l+i),tc.g,0,n))
 I(w>tw,C*p=tabs+tw;memset(p,0,w-tw);W(--p>tabs&&!*p)for(p+=8;p<tabs+w;p+=8)*p=1)tw=w;th=h;ll=n;tscrl(0,h-1);tmv(tc.x,tc.y);tDIRT();}
S V trst(){G g={.f=df,.b=db};memset(tabs,0,tw);for(I i=8;i<tw;i+=8)tabs[i]=1;ty0=0;ty1=th-1;tm=TW|TU;memset(tran,CSUS,SZ tran);tcs=0;
 i(2,V(s,scr+i)s->sc=(Cur){{.f=df,.b=db}};s->cur=0;s->o=0;j(th,cll(el(s->l+j),g,0,tw))F(j,th,s->n,free(s->l[j]);s->l[j]=0))tcur(1);ll=tw;tDIRT();}
S V tnew(I w,I h){scr[0].n=HSZ;tsz(w,h);trst();}
S I talt()_(tM(TA))
S V tswp(){tm^=TA;tDIRT();}
S V up(I n){P(tM(TA))n=MN(n,SCR.n-th-SCR.o);W(!TL(-n),--n)SCR.o+=n;selsc(0,n);tDIRT();} S V up1(){up(1);} S V upN(){up(th);}
S V dn(I n){P(tM(TA))n=MN(n,SCR.o);SCR.o-=n;selsc(0,-n);tDIRT();}                       S V dn1(){dn(1);} S V dnN(){dn(th);}
S V tdn(I o,I n){LM(n,ty1-o)eL(&TL(-n),n);I i=ty1;W(++i<th,SWP(TL(i),TL(i-n)))i(o,SWP(TL(i),TL(i-n)))SCR.cur=(SCR.cur+SCR.n-n)%SCR.n;tcr(0,o,ll-1,o+n-1);tdirt(o+n-1,ty1);selsc(o,n);}
S V tup(I o,I n){LM(n,ty1-o)eL(&TL(th),n);for(I i=o-1;i>=0;i--)SWP(TL(i),TL(i+n));for(I i=th-1;i>ty1;i--)SWP(TL(i),TL(i+n));SCR.cur=(SCR.cur+n)%SCR.n;tcr(0,ty1-n+1,ll-1,ty1);
 tdirt(o,ty1-n+1);selsc(o,-n);}
S V tnl(I c0){I y=tc.y;y==ty1?tup(ty0,1):y++;tmv(tc.x*!c0,y);}
S V tmva(I x,I y){tmv(x,y+(tc.s&2?ty0:0));}
S V tmv(I x,I y){I y0,y1;I(tc.s&2,y0=ty0;y1=ty1)E(y0=0;y1=th-1)tc.s&=~1;tc.x=LM(x,tw)tc.y=MX(y0,MN(y1,y));}
S V tsc(UI u,O G*a,I x,I y){S C*t[]={"↑","↓","→","←","█","▚","☃",[30]=" ","◆","▒","␉","␌","␍","␊","°","±","␤","␋","┘","┐","┌","└","┼","⎺","⎻","─","⎼","⎽","├","┤","┴","┬","│","≤","≥","π","≠","£","·"};
 G*l=TL(y);I(!tran[tcs]&&u-65<62u&&t[u-65],u8d(t[u-65],&u,4))I(l[x].a&AWi,I(x+1<tw,l[x+1].u=32;l[x+1].a&=~AD))J(l[x].a&AD,l[x-1].u=32;l[x-1].a&=~AWi)dty[y]=1;l[x]=*a;l[x].u=u;}
S V tcr(I x0,I y0,I x1,I y1){I(x0>x1,SWP(x0,x1))I(y0>y1,SWP(y0,y1))LM(x0,ll)LM(x1,ll)LM(y0,th)LM(y1,th)I l=TLO(y0);
 memset(dty+y0,1,y1-y0+1);F(y,y0,y1+1,F(x,x0,x1+1,G*g=SCR.l[l]+x;I(sel(x,y),selcl())g->f=tc.g.f;g->b=tc.g.b;g->a=0;g->u=32)l=(l+1)%SCR.n)}
S V til(I n){I(ty0<=tc.y&&tc.y<=ty1,tdn(tc.y,n))}S V tib(I n){LM(n,tw-tc.x+1)I q=tc.x+n,p=tc.x,m=tw-q;G*l=TL(tc.y);memmove(l+q,l+p,m*SZ*l);tcr(p,tc.y,q-1,tc.y);}
S V tdl(I n){I(ty0<=tc.y&&tc.y<=ty1,tup(tc.y,n))}S V tdc(I n){LM(n,tw-tc.x+1)I q=tc.x,p=tc.x+n,m=tw-p;G*l=TL(tc.y);memmove(l+q,l+p,m*SZ*l);tcr(tw-n,tc.y,tw-1,tc.y);}
S I tdefclr(O I*a,I*n,I l)_(I i=-1;UI r,g,b;SW(a[*n+1],{PE("unknown gfx attr:%d\n",a[*n]);},
 C(2,I(*n+4>=l,PE("incorrect number of parameters:%d\n",*n))E(r=a[*n+2];g=a[*n+3];b=a[*n+4];*n+=4;I((r|g|b)>>8,PE("bad rgb colour:(%u,%u,%u)\n",r,g,b))E(i=1<<24|r<<16|g<<8|b)))
 C(5,I(*n+2>=l,PE("incorrect number of parameters:%d\n",*n))E(*n+=2;I(a[*n]>>8,PE("bad fgcolour:%d\n",a[*n]))E(i=a[*n]))))i)
S V tsa(O I*a,I n){i(n,I x=a[i];
 I(x- 1<8u,tc.g.a|= (I[]){AB,AF,AI,AU,ABl,ABl,AR,AIn|AS}[x-1])                    J(x-30<8u,tc.g.f=x-30)J(x==38,I j=tdefclr(a,&i,n);I(j>=0,tc.g.f=j))J(x==39,tc.g.f=df)J(x- 90<8u,tc.g.f=x- 90+8)
 J(x-22<8u,tc.g.a&=~(I[]){AB|AF,AI,AU,ABl,0  ,AR,AIn,AS}[x-22])                   J(x-40<8u,tc.g.b=x-40)J(x==48,I j=tdefclr(a,&i,n);I(j>=0,tc.g.b=j))J(x==49,tc.g.b=db)J(x-100<8u,tc.g.b=x-100+8)
 J(!x     ,tc.g.a&=~     (AB|AF|AI|AU|ABl    |AR|AIn|AS);tc.g.f=df;tc.g.b=db) E(PE("unknown gfx attr:%d\n",x);csiD()))}
S V tsm(I p,I s,O I*a,I n){P(!p,i(n,I x=a[i];SW(x,PE("unknown set/reset mode:%d\n",x),C(0)C(2,xm(s,WK))C(4,BT(tm,s,TI))C(12,BT(tm,!s,TE))C(20,BT(tm,s,TC))));)
 i(n,I x=a[i];SW(x,PE("unknown private set/reset mode:%d\n",x),C5(0,2,3,4,8,C5(12,18,19,42,1001,C2(1005,1015,BR)))C(1,xm(s,WAC))C(5,xm(s,WR))C(1004,xm(s,Wf))C(1006,xm(s,WMG))C(1034,xm(s,W8))
  C(2004,xm(s,WBP))C(25,xm(!s,WH))C(6,BT(tc.s,s,2);tmva(0,0))C(7,BT(tm,s,TW))C(9,xpm(0);xm(0,WMM);xm(s,WX))C(1000,xpm(0);xm(0,WMM);xm(s,WMB))C(1002,xpm(0);xm(0,WMM);xm(s,WMMo))
  C(1003,xpm(s);xm(0,WMM);xm(s,WMMa))C(1049,tcur(!s);/*thru*/C2(47,1047,I alt=tM(TA);I(alt,tcr(0,0,tw-1,th-1))I(s^alt,tswp())I(x-1049,BR)/*thru*/C(1048,tcur(!s))))))}

//CSI:control sequence introducer {b:buffer,n:buffer,length,priv,args,nargs,mode}, csiD:dump, csir:reset, csip:parse
S ST{C b[512];N n;C p;I a[16],na;C m[2];}csi;S V csir(){memset(&csi,0,SZ csi);}S V csiD(){PE("ESC[");i(csi.n,UC c=csi.b[i];isprint(c)?putc(c,stderr):PE("(%02x)",c))putc(10,stderr);}
S V csip(){C*p=csi.b,*np;csi.na=0;I(*p=='?',csi.p=1;p++)csi.b[csi.n]=0;
 W(p<csi.b+csi.n,np=0;V(v,strtol(p,&np,10))I(np==p,v=0)I(v==-1ull>>1||v==-1ull<<63,v=-1)csi.a[csi.na++]=v;p=np;I(*p-';'||csi.na==LEN(csi.a),BR)p++)*csi.m=*p++;csi.m[1]=p<csi.b+csi.n?*p:0;}
S V csih(){I x=*csi.a,y=csi.a[1];C m=*csi.m;SW(m,u:PE("unknown csi");csiD(),C('@',tib(x|!x))C('A',tmv(tc.x,tc.y-x-!x))C2('B','e',tmv(tc.x,tc.y+x+!x))C2('C','a',tmv(tc.x+x+!x,tc.y))
 C('D',tmv(tc.x-x-!x,tc.y))C('E',tmv(0,tc.y+x+!x))C('F',tmv(0,tc.y-x-!x))C2('G','`',tmv(x+!x-1,tc.y))C2('H','f',tmva(y+!y-1,x+!x-1))C('I',ttab(x|!x))
 C('J',SW(x,goto u,C(0,tcr(tc.x,tc.y,tw-1,tc.y);I(tc.y<th-1,tcr(0,tc.y+1,tw-1,th-1)))C(1,I(tc.y>1,tcr(0,0,tw-1,tc.y-1))tcr(0,tc.y,tc.x,tc.y))C(2,tcr(0,0,tw-1,th-1))))
 C('K',I(x>2u,goto u)tcr(tc.x*!x,tc.y,x-1?tw-1:tc.x,tc.y))C('L',til(x|!x))C('M',tdl(x|!x))C('P',tdc(x|!x))C('S',tup(ty0,x|!x))C('T',tdn(ty0,x|!x))C('X',tcr(tc.x,tc.y,tc.x+x+!x-1,tc.y))
 C('Z',ttab(-x-!x))C('b',x|=!x;I(lc,W(x-->0,tpc(lc))))C('c',I(!*csi.a,tid()))C('d',tmva(tc.x,x+!x-1))C('g',SW(x,goto u,C(0,tabs[tc.x]=0)C(3,memset(tabs,0,tw))))
 C('h',tsm(csi.p,1,csi.a,csi.na))C('i',SW(x,goto u,C(0,tD())C(1,tDl(tc.y))C(2,tDs())C(4,tm&=~TP)C(5,tm|=TP)))C('l',tsm(csi.p,0,csi.a,csi.na))C('m',tsa(csi.a,csi.na))
 C('n',I(x==6,C s[40];ttyw(s,snprintf(s,SZ s,"\e[%i;%iR",tc.y+1,tc.x+1),0)))C('r',I(csi.p,goto u)tscrl(x+!x-1,(y?y:th)-1);tmva(0,0))C2('s','u',tcur(m=='u'))C(32,I(csi.m[1]-'q'||xsc(x),goto u)))}

//STR  {b:buffer,a:args,t:type,n:bufferlength,na:nargs}, strD:dump, strr:reset, strp:parse, strh:handler
S ST{C b[512],*a[16],t;I n,na;}str;S V strr(){memset(&str,0,SZ str);}S V strD(){PE("ESC%c",str.t);i(str.n,UC c=str.b[i];!c?putc(10,stderr):isprint(c)?putc(c,stderr):PE("(%02x)",c))PE("ESC\\\n");}
S V strp(){C*p=str.b;str.na=0;str.b[str.n]=0;P(!*p)W(str.na<LEN(str.a),str.a[str.na++]=p;I c;W((c=*p)-';'&&c,p++)P(!c)*p++=0;)}
S V osc(I i,I x,C*v){UC r,g,b;P(xgc(i,&r,&g,&b),PE("can't fetch osc colour:%d\n",i);{})C s[32];ttyw(s,snprintf(s,SZ s,"\e]%s%d;rgb:%02x%02x/%02x%02x/%02x%02x\a",v,x,r,r,g,g,b,b),1);}
S V strh(){esc&=~EE&~ES;strp();C*x=*str.a,*y=str.a[1];I na=str.na,i=na?atoi(x):0;
 SW(str.t,,C('k',xst(x);R)C3('P','_','^',R)C(']',SW(i,,C3(0,1,2,I(na>1,I(i-1,xst(y))I(i<2,xsi(y)))R)C(52,R)
  C3(10,11,12,I(na<2,BR)I d=(I[]){df,db,dcs}[i-10];!strcmp(y,"?")?osc(d,i,""):xcn(d,y)?PE("invalid colour:%s\n",y):rdr();R)
  C(4,I(na<3,BR)/*thru*/C(104,I j=na>1?atoi(y):-1;y&&!strcmp(y,"?")?osc(j,j,"4;"):xcn(j,y)?(i==104&&na<2?0:PE("invalid colour j=%d,y=%s\n",j,y)):rdr();R)))))
 PE("unknown str ");strD();}

//MORE TERMINAL  tp:print, ttp:toggle print, tDs:dump selection, tDl:dump line, tD:dump, tu8:utf8, tdt:define translation, tT:DEC test, tss:str sequence, tcc:control char, esch:escape handler,
//tpc:put char, twr:write, rdr:redraw
S V brk(){I(tcsendbreak(cfd,0),PE("error sending break\n"))}
S V tp(C*s,N n){P(iofd<0)W(n>0,I k=write(iofd,s,n);P(k<0,PE("error writing to output file\n");close(iofd);iofd=-1;{})n-=k;s+=k)}S V ttp(){tm^=TP;}
S V tDs(){C*s=gsel();I(s,tp(s,strlen(s));free(s))}S V tDl(I n){O G*p=TL(n),*q=p+MN(ly(n),tw)-1;I(p-q||p->u-32,C s[4];for(;p<=q;p++)tp(s,u8e(p->u,s)))tp("\n",1);}S V tD(){i(th,tDl(i))}
S V tu8(C c){c=='G'?tm|=TU:c=='@'?tm&=~TU:0;}S V tdt(C c){C*s="0B",*p=strchr(s,c);I(!p,PE("esc unhandled charset: ESC ( %c\n",c))E(tran[tics]=(I[]){0,CSUS}[p-s])}
S V ttab(I n){UI x=tc.x;I(n>0,W(x<tw&&n--,x++;W(x<tw&&!tabs[x],x++)))J(n<0,W(x>0&&n++,x--;W(x>0&&!tabs[x],x--)))tc.x=LM(x,tw)}S V tT(C c){P(c-'8')i(tw,j(th,tsc('E',&tc.g,i,j)))}
S V tss(C c){C*s="\x90\x9f\x9e\x9d",*p=strchr(s,c);I(p,c="P_^]"[p-s])strr();str.t=c;esc|=ES;}
S V tcc(UC c){SW(c,,C5(0,5,17,19,127,R)C(7,esc&EE?strh():xbe())C(8,tmv(tc.x-1,tc.y);R)C(9,ttab(1);R)C3(10,11,12,tnl(tM(TC));R)C(13,tmv(0,tc.y);R)C2(14,15,tcs=15-c;R)C(24,csir())
 C(27,csir();esc&=~(EC|EA|ET);esc|=E0;R)C(26,tsc('?',&tc.g,tc.x,tc.y);csir())C(0x85,tnl(1))C(0x88,tabs[tc.x]=1)C(0x9a,tid())C4(0x90,0x9d,0x9e,0x9f,tss(c);R))esc&=~EE&~ES;}
S I esch(UC x)_(SW(x,PE("unknown sequence: ESC 0x%02X '%c'\n",x,isprint(x)?x:'.'),C('[',esc|=EC;R 0)C('#',esc|=ET;R 0)C('%',esc|=EU;R 0)C5('P','_','^',']','k',tss(x);R 0)
 C2('n','o',tcs=2+x-'n')C4('(',')','*','+',tics=x-'(';esc|=EA;R 0)C('D',tc.y==ty1?tup(ty0,1):tmv(tc.x,tc.y+1))C('E',tnl(1))C('H',tabs[tc.x]=1)C('M',tc.y==ty0?tdn(ty0,1):tmv(tc.x,tc.y-1))C('Z',tid())
 C('c',trst();xst(0);xlC())C2('=','>',xm(x&1,WAK))C2('7','8',tcur(x-'7'))C('\\',I(esc&EE,strh())))1)
S V tpc(UI u){C c[4];I w,n,ctrl=cc(u);I(u<127||!tM(TU),*c=u;w=n=1)E(n=u8e(u,c);I(!ctrl&&(w=wcwidth(u))<0,w=1))I(tM(TP),tp(c,n))
 P(esc&ES,I(u==7||u==24||u==26||u==27||cc1(u),esc&=~(E0|ES);esc|=EE;goto chk)Q(str.n+n<SZ str.b);memmove(str.b+str.n,c,n);str.n+=n;{})
 chk:P(ctrl,tcc(u);I(!esc,lc=0){})P(esc&E0,P(esc&EC,csi.b[csi.n++]=u;I(u-64<63u||csi.n>=SZ(csi.b)-1,esc=0;csip();csih()))I(esc&EU,tu8(u))J(esc&EA,tdt(u))J(esc&ET,tT(u))E(P(!esch(u)))esc=0;{})
 I(sel(tc.x,tc.y),selcl())G*gp=TL(tc.y)+tc.x;I(tM(TW)&&(tc.s&1),gp->a|=AWr;tnl(1);gp=TL(tc.y)+tc.x)I(tM(TI)&&tc.x+w<tw,memmove(gp+w,gp,(tw-tc.x-w)*SZ(G)))I(tc.x+w>tw,tnl(1);gp=TL(tc.y)+tc.x)
 tsc(u,&tc.g,tc.x,tc.y);lc=u;I(w==2,gp->a|=AWi;I(tc.x+1<tw,I(gp[1].a==AWi&&tc.x+2<tw,gp[2].u=32;gp[2].a&=~AD)gp[1].u=0;gp[1].a=AD))I(tc.x+w<tw,tmv(tc.x+w,tc.y))E(tc.s|=1)}
S I twr(O UC*s,I n,I c)_(I(SCR.o,SCR.o=0;tDIRT())UI u;I k;
 for(I r=0;r<n;r+=k){I(tM(TU),k=u8d(s+r,&u,n-r);P(!k,r))E(u=s[r];k=1)I(c&&cc(u),u&128?u&=127,tpc('^'),tpc('['):u-9&&u-10&&u-13?u^=64,tpc('^'):0)tpc(u);}n)
S V draw(){S I px,py;I cx=tc.x,qx=px,qy=py;P(!xd1())LM(px,tw)LM(py,th)I(TL(py)[px].a&AD,px--)I(TL(tc.y)[cx].a&AD,cx--)I l=TLO(0);i(th,I(dty[i],dty[i]=0;xdl(SCR.l[l],0,i,tw))l=(l+1)%SCR.n)
 I(!SCR.o,xdc(cx,tc.y,TL(tc.y)[cx],px,py,TL(py)[px]))px=cx;py=tc.y;xd0();I(qx-px||qy-py,xxs(px,py))}
S V rdr(){tDIRT();draw();}

//GUI  cnm:colour name, bp:border pixels, FM:force mouse mode, key{keysymbol,mask,string,appkey,appcursor}
#include<locale.h>
#include<time.h>
#include<X11/Xatom.h>
#include<X11/cursorfont.h>
#include<X11/Xft/Xft.h>
#include<X11/XKBlib.h>
S O I _C=ControlMask,_S=ShiftMask,CS=_C|_S,_1=Mod1Mask,_3=Mod3Mask,_4=Mod4Mask,S1=_S|_1,C1=_C|_1,X1=CS|_1,XX=~0,M=-1;//modifiers
S V0 xcp,xpa,nlk,spa,zi,zo,zr,ts0,ts1,ts2,ts3,up1,upN,dn1,dnN;S O C*cnm[]={CLR};S UI bp=2,FM=ShiftMask;
S ST{UI m,btn;V0*f;UI release;I altscrn;}mshc[]={{XX,4,up1,0,-1},{XX,5,dn1,0,-1},{XX,2,spa,1},{_S,4,ts0},{XX,4,ts1},{_S,5,ts2},{XX,5,ts3}};
S ST{UI m;KeySym k;V0*f;}shc[]={{XX,XK_Break,brk},{_C,XK_Print,ttp},{_S,XK_Print,tD},{XX,XK_Print,tDs},{CS,XK_Prior,zi},{CS,XK_Next,zo},{CS,XK_plus,zi},{_C,XK_minus,zo},{CS,XK_Home,zr},{CS,XK_C,xcp},
 {CS,XK_V,xpa},{CS,XK_Y,spa},{_S,XK_Insert,spa},{CS,XK_Num_Lock,nlk},{_S,XK_Page_Up,upN},{_S,XK_Page_Down,dnN}};
S ST{KeySym k;UI m;C*s,ak,ac;}key[]={{XK_KP_Home,_S,"\e[2J",0,M},{XK_KP_Home,_S,"\e[1;2H",0,1},{XK_KP_Home,XX,"\e[H",0,M},{XK_KP_Home,XX,"\e[1~",0,1},{XK_KP_Up,XX,"\e",1},{XK_KP_Up,XX,"\e[A",0,M},
 {XK_KP_Up,XX,"\eOA",0,1},{XK_KP_Down,XX,"\eOr",1},{XK_KP_Down,XX,"\e[B",0,M},{XK_KP_Down,XX,"\eOB",0,1},{XK_KP_Left,XX,"\eOt",1},{XK_KP_Left,XX,"\e[D",0,M},{XK_KP_Left,XX,"\eOD",0,1},
 {XK_KP_Right,XX,"\eOv",1},{XK_KP_Right,XX,"\e[C",0,M},{XK_KP_Right,XX,"\eOC",0,1},{XK_KP_Prior,_S,"\e[5;2~"},{XK_KP_Prior,XX,"\e[5~"},{XK_KP_Begin,XX,"\e[E"},{XK_KP_End,_C,"\e[J",M},
 {XK_KP_End,_C,"\e[1;5F",1},{XK_KP_End,_S,"\e[K",M},{XK_KP_End,_S,"\e[1;2F",1},{XK_KP_End,XX,"\e[4~"},{XK_KP_Next,_S,"\e[6;2~"},{XK_KP_Next,XX,"\e[6~"},{XK_KP_Insert,_S,"\e[2;2~",1},
 {XK_KP_Insert,_S,"\e[4l",M},{XK_KP_Insert,_C,"\e[L",M},{XK_KP_Insert,_C,"\e[2;5~",1},{XK_KP_Insert,XX,"\e[4h",M},{XK_KP_Insert,XX,"\e[2~",1},{XK_KP_Delete,_C,"\e[M",M},{XK_KP_Delete,_C,"\e[3;5~",1},
 {XK_KP_Delete,_S,"\e[2K",M},{XK_KP_Delete,_S,"\e[3;2~",1},{XK_KP_Delete,XX,"\e[P",M},{XK_KP_Delete,XX,"\e[3~",1},{XK_KP_Multiply,XX,"\eOj",2},{XK_KP_Add,XX,"\eOk",2},{XK_KP_Enter,XX,"\eOM",2},
 {XK_KP_Enter,XX,"\r",M},{XK_KP_Subtract,XX,"\eOm",2},{XK_KP_Decimal,XX,"\eOn",2},{XK_KP_Divide,XX,"\eOo",2},{XK_KP_0,XX,"\eOp",2},{XK_KP_1,XX,"\eOq",2},{XK_KP_2,XX,"\eOr",2},{XK_KP_3,XX,"\eOs",2},
 {XK_KP_4,XX,"\eOt",2},{XK_KP_5,XX,"\eOu",2},{XK_KP_6,XX,"\eOv",2},{XK_KP_7,XX,"\eOw",2},{XK_KP_8,XX,"\eOx",2},{XK_KP_9,XX,"\eOy",2},{XK_Up,_S,"\e[1;2A"},{XK_Up,_1,"\e[1;3A"},{XK_Up,S1,"\e[1;4A"},
 {XK_Up,_C,"\e[1;5A"},{XK_Up,CS,"\e[1;6A"},{XK_Up,C1,"\e[1;7A"},{XK_Up,X1,"\e[1;8A"},{XK_Up,XX,"\e[A",0,M},{XK_Up,XX,"\eOA",0,1},{XK_Down,_S,"\e[1;2B"},{XK_Down,_1,"\e[1;3B"},{XK_Down,S1,"\e[1;4B"},
 {XK_Down,_C,"\e[1;5B"},{XK_Down,CS,"\e[1;6B"},{XK_Down,C1,"\e[1;7B"},{XK_Down,X1,"\e[1;8B"},{XK_Down,XX,"\e[B",0,M},{XK_Down,XX,"\eOB",0,1},{XK_Left,_S,"\e[1;2D"},{XK_Left,_1,"\e[1;3D"},
 {XK_Left,S1,"\e[1;4D"},{XK_Left,_C,"\e[1;5D"},{XK_Left,CS,"\e[1;6D"},{XK_Left,C1,"\e[1;7D"},{XK_Left,X1,"\e[1;8D"},{XK_Left,XX,"\e[D",0,M},{XK_Left,XX,"\eOD",0,1},{XK_Right,_S,"\e[1;2C"},
 {XK_Right,_1,"\e[1;3C"},{XK_Right,S1,"\e[1;4C"},{XK_Right,_C,"\e[1;5C"},{XK_Right,CS,"\e[1;6C"},{XK_Right,C1,"\e[1;7C"},{XK_Right,X1,"\e[1;8C"},{XK_Right,XX,"\e[C",0,M},{XK_Right,XX,"\eOC",0,1},
 {XK_ISO_Left_Tab,_S,"\e[Z"},{XK_Return,_1,"\e\r"},{XK_Return,XX,"\r"},{XK_Insert,_S,"\e[4l",M},{XK_Insert,_S,"\e[2;2~",1},{XK_Insert,_C,"\e[L",M},{XK_Insert,_C,"\e[2;5~",1},{XK_Insert,XX,"\e[4h",M},
 {XK_Insert,XX,"\e[2~",1},{XK_Delete,_C,"\e[M",M},{XK_Delete,_C,"\e[3;5~",1},{XK_Delete,_S,"\e[2K",M},{XK_Delete,_S,"\e[3;2~",1},{XK_Delete,XX,"\e[P",M},{XK_Delete,XX,"\e[3~",1},
 {XK_BackSpace,0,"\177"},{XK_BackSpace,_1,"\e\177"},{XK_Home,_S,"\e[2J",0,M},{XK_Home,_S,"\e[1;2H",0,1},{XK_Home,XX,"\e[H",0,M},{XK_Home,XX,"\e[1~",0,1},{XK_End,_C,"\e[J",M},{XK_End,_C,"\e[1;5F",1},
 {XK_End,_S,"\e[K",M},{XK_End,_S,"\e[1;2F",1},{XK_End,XX,"\e[4~"},{XK_Prior,_C,"\e[5;5~"},{XK_Prior,_S,"\e[5;2~"},{XK_Prior,XX,"\e[5~"},{XK_Next,_C,"\e[6;5~"},{XK_Next,_S,"\e[6;2~"},
 {XK_Next,XX,"\e[6~"},{XK_F1,0,"\eOP"},{XK_F1,_S,"\e[1;2P"},{XK_F1,_C,"\e[1;5P"},{XK_F1,_4,"\e[1;6P"},{XK_F1,_1,"\e[1;3P"},{XK_F1,_3,"\e[1;4P"},{XK_F2,0,"\eOQ"},{XK_F2,_S,"\e[1;2Q"},
 {XK_F2,_C,"\e[1;5Q"},{XK_F2,_4,"\e[1;6Q"},{XK_F2,_1,"\e[1;3Q"},{XK_F2,_3,"\e[1;4Q"},{XK_F3,0,"\eOR"},{XK_F3,_S,"\e[1;2R"},{XK_F3,_C,"\e[1;5R"},{XK_F3,_4,"\e[1;6R"},{XK_F3,_1,"\e[1;3R"},
 {XK_F3,_3,"\e[1;4R"},{XK_F4,0,"\eOS"},{XK_F4,_S,"\e[1;2S"},{XK_F4,_C,"\e[1;5S"},{XK_F4,_4,"\e[1;6S"},{XK_F4,_1,"\e[1;3S"},{XK_F5,0,"\e[15~"},{XK_F5,_S,"\e[15;2~"},{XK_F5,_C,"\e[15;5~"},
 {XK_F5,_4,"\e[15;6~"},{XK_F5,_1,"\e[15;3~"},{XK_F6,0,"\e[17~"},{XK_F6,_S,"\e[17;2~"},{XK_F6,_C,"\e[17;5~"},{XK_F6,_4,"\e[17;6~"},{XK_F6,_1,"\e[17;3~"},{XK_F7,0,"\e[18~"},{XK_F7,_S,"\e[18;2~"},
 {XK_F7,_C,"\e[18;5~"},{XK_F7,_4,"\e[18;6~"},{XK_F7,_1,"\e[18;3~"},{XK_F8,0,"\e[19~"},{XK_F8,_S,"\e[19;2~"},{XK_F8,_C,"\e[19;5~"},{XK_F8,_4,"\e[19;6~"},{XK_F8,_1,"\e[19;3~"},{XK_F9,0,"\e[20~"},
 {XK_F9,_S,"\e[20;2~"},{XK_F9,_C,"\e[20;5~"},{XK_F9,_4,"\e[20;6~"},{XK_F9,_1,"\e[20;3~"},{XK_F10,0,"\e[21~"},{XK_F10,_S,"\e[21;2~"},{XK_F10,_C,"\e[21;5~"},{XK_F10,_4,"\e[21;6~"},
 {XK_F10,_1,"\e[21;3~"},{XK_F11,0,"\e[23~"},{XK_F11,_S,"\e[23;2~"},{XK_F11,_C,"\e[23;5~"},{XK_F11,_4,"\e[23;6~"},{XK_F11,_1,"\e[23;3~"},{XK_F12,0,"\e[24~"},{XK_F12,_S,"\e[24;2~"},
 {XK_F12,_C,"\e[24;5~"},{XK_F12,_4,"\e[24;6~"},{XK_F12,_1,"\e[24;3~"},{XK_F13,0,"\e[1;2P"},{XK_F14,0,"\e[1;2Q"},{XK_F15,0,"\e[1;2R"},{XK_F16,0,"\e[1;2S"},{XK_F17,0,"\e[15;2~"},{XK_F18,0,"\e[17;2~"},
 {XK_F19,0,"\e[18;2~"},{XK_F20,0,"\e[19;2~"},{XK_F21,0,"\e[20;2~"},{XK_F22,0,"\e[21;2~"},{XK_F23,0,"\e[23;2~"},{XK_F24,0,"\e[24;2~"},{XK_F25,0,"\e[1;5P"},{XK_F26,0,"\e[1;5Q"},{XK_F27,0,"\e[1;5R"},
 {XK_F28,0,"\e[1;5S"},{XK_F29,0,"\e[15;5~"},{XK_F30,0,"\e[17;5~"},{XK_F31,0,"\e[18;5~"},{XK_F32,0,"\e[19;5~"},{XK_F33,0,"\e[20;5~"},{XK_F34,0,"\e[21;5~"},{XK_F35,0,"\e[23;5~"}};

#define XR(a...) XftDrawRect(drw,a)
//Fnt:{h:height,w:width,asc:ascent,badsl:bad slant?,badwt:bad weight?,m:match}, xws:screen, xwf:is fixed geometry?, xwx:left, xwy:top, xwg:geometry mask, wi:window info,
//fnts[0=normal,1=italic,2=bold,3=bolditalic], pri:primary, clb:clipboard, tc0,tc1:click times, gc:graphics context, fc:font cache{f:font,m:flags,u:unicodepoint},
//ufs,dfs:used&default font sizes, uf:used font
TD XftColor XC;TD XftGlyphFontSpec GFS;TD ST{I h,w,asc,badsl,badwt;XftFont*m;FcFontSet*set;FcPattern*p;}Fnt;S Atom aE,aD,aN,aI,aU,aC,aX,aT;S Window xw;S I xws,xwf,xwx,xwy,xwg;
S ST{I tw,th,w,h,ch,cw,m,cur;}wi;S Fnt fnts[4];S XC clrs[1024];S N nclr;S Drawable dwb;S C*pri,*cbd;S ST timespec tc0,tc1;S Display*dp;S Colormap cm;S GFS gfs[MXW];S XIM xim;S XIC xic;S XPoint spot;
S XVaNestedList spots;S Visual*vi;S XftDraw*drw;S GC gc;S XSetWindowAttributes ats;S ST{XftFont*f;UI m,u;}fc[256];S I nfc;S double ufs,dfs;S C*uf;S UI btns;

//xi:intern atom, wM:test window mode bit, mtc:match, nlk:toggle numlock, cgt:clock get time, xcp:copy, xpa:paste, spa:selection paste, xcl:clear, xsz,csz:resize,
//xuf:unload font, xuF:unload fonts, xlf:load font, xlF:load fonts, xgr:gravity, xhs:hints, zm:zoom, zi:zoom in, zo:zoom out, zr:zoom reset, ts_,ts0,..:tty send
//xlc:load colour, xlC:load colours, xgc:get colour, xcn:set colour name, xic0:XIC destroy, xpc:parse colour, xmG:make glyph font spec, xdG:draw glyph font spec, xdg:draw glyph, xdc:draw cursor,
//xsi:set icon title, xst:set title, xd1:start draw, xd0:end draw, xxs:set X input method spot, xpm:set pointer motion, xm:set window mode, xsc:set cursor, xsu:set urgent, xbe:bell
S Atom xi(C*s)_(XInternAtom(dp,s,0))S I wM(I x)_(!!(wi.m&x))S I mtc(UI x,UI y)_(x==~0||x==(y&~(Mod2Mask|1<<13|1<<14)))S V nlk(){wi.m^=WN;}S V cgt(ST timespec*t){QN(clock_gettime(CLOCK_MONOTONIC,t));}
S V xcp(){free(cbd);cbd=0;I(pri,cbd=Q(strdup(pri));XSetSelectionOwner(dp,aC,xw,CurrentTime))}S V xpa(){XConvertSelection(dp,aC,aU,aC,xw,CurrentTime);}
S V spa(){XConvertSelection(dp,XA_PRIMARY,aU,XA_PRIMARY,xw,CurrentTime);}S V xcl(I x0,I y0,I x1,I y1){XR(clrs+df+!wM(WR),x0,y0,x1-x0,y1-y0);}
S V xsz(I w,I h){wi.tw=w*wi.cw;wi.th=h*wi.ch;XFreePixmap(dp,dwb);dwb=XCreatePixmap(dp,xw,wi.w,wi.h,DefaultDepth(dp,xws));XftDrawChange(drw,dwb);xcl(0,0,wi.w,wi.h);}
S V csz(I w,I h){I(w,wi.w=w)I(h,wi.h=h)I c=MX(1,(wi.w-2*bp)/wi.cw),r=MX(1,(wi.h-2*bp)/wi.ch);tsz(c,r);xsz(c,r);ttysz(wi.tw,wi.th);}
S V xuf(Fnt*f){XftFontClose(dp,f->m);FcPatternDestroy(f->p);I(f->set,FcFontSetDestroy(f->set))}S V xuF(){W(nfc>0,XftFontClose(dp,fc[--nfc].f))i(4,xuf(fnts+i))}
S I xlf(Fnt*f,FcPattern*p)_(FcPattern*c=FcPatternDuplicate(p);P(!c,1)FcConfigSubstitute(0,c,FcMatchPattern);XftDefaultSubstitute(dp,xws,c);
 FcResult r;FcPattern*m=FcFontMatch(0,c,&r);P(!m,FcPatternDestroy(c);1)P(!(f->m=XftFontOpenPattern(dp,m)),FcPatternDestroy(c);FcPatternDestroy(m);1) I want,have;
 I(!XftPatternGetInteger(p,"slant" ,0,&want)&&(XftPatternGetInteger(f->m->pattern,"slant" ,0,&have)||have<want),f->badsl=1;PE("font slant does not match\n" ))
 I(!XftPatternGetInteger(p,"weight",0,&want)&&(XftPatternGetInteger(f->m->pattern,"weight",0,&have)||have-want),f->badwt=1;PE("font weight does not match\n"))
 S C a[96];I(!*a,i(95,a[i]=32+i)a[95]=0)XGlyphInfo gi;XftTextExtentsUtf8(dp,f->m,a,strlen(a),&gi);f->set=0;f->p=c;f->asc=f->m->ascent;f->h=f->asc+f->m->descent;N n=strlen(a);f->w=(gi.xOff+n-1)/n;0)
S V xlF(O C*s,double sz){double v;FcPattern*p=Q(*s-'-'?FcNameParse(s):XftXlfdParse(s,0,0));
 I(sz>1,FcPatternDel(p,FC_PIXEL_SIZE);FcPatternDel(p,FC_SIZE);FcPatternAddDouble(p,FC_PIXEL_SIZE,sz);ufs=sz)
 E(dfs=ufs=!FcPatternGetDouble(p,FC_PIXEL_SIZE,0,&v)?v:!FcPatternGetDouble(p,FC_SIZE,0,&v)?-1:(FcPatternAddDouble(p,FC_PIXEL_SIZE,12),12))
 Q(!xlf(&fnts[0],p));I(ufs<0,FcPatternGetDouble(fnts[0].m->pattern,FC_PIXEL_SIZE,0,&v);ufs=v;I(!sz,dfs=v))wi.cw=fnts[0].w;wi.ch=fnts[0].h;
 i(3,C*o=i<2?FC_SLANT:FC_WEIGHT;FcPatternDel(p,o);FcPatternAddInteger(p,o,!i?FC_SLANT_ITALIC:i==1?FC_SLANT_ROMAN:FC_WEIGHT_BOLD);Q(!xlf(fnts+1+i,p)))FcPatternDestroy(p);}
S I xgr(I m)_(m&=XNegative|YNegative;!m?NorthWestGravity:m==XNegative?NorthEastGravity:m==YNegative?SouthWestGravity:SouthEastGravity)
S V xhs(){XClassHint c={tnm,tnm};XWMHints wm={.flags=InputHint,.input=1};XSizeHints*h=XAllocSizeHints();h->flags=PSize|PResizeInc|PBaseSize|PMinSize;h->height=wi.h;h->width=wi.w;h->height_inc=wi.ch;
 h->width_inc=wi.cw;h->base_height=2*bp;h->base_width=2*bp;h->min_height=wi.ch+2*bp;h->min_width=wi.cw+2*bp;I(xwf,h->flags|=PMaxSize;h->min_width=h->max_width=wi.w;h->min_height=h->max_height=wi.h)
 I(xwg&(XValue|YValue),h->flags|=USPosition|PWinGravity;h->x=xwx;h->y=xwy;h->win_gravity=xgr(xwg))XSetWMProperties(dp,xw,0,0,0,0,h,&wm,&c);XFree(h);}
S V zm(double x){xuF();xlF(uf,x);csz(0,0);rdr();xhs();}S V zi(){zm(ufs+1);}S V zo(){zm(ufs-1);}S V zr(){I(dfs,zm(dfs))}
S V ts_(O C*s){ttyw(s,strlen(s),1);}S V ts0(){ts_("\e[5;2~");}S V ts1(){ts_("1");}S V ts2(){ts_("\e[6;2~");}S V ts3(){ts_("\x05");}
S UH d6(I x)_(x%=6;x?0x3737+0x2828*x:0)
S I xlc(I i,O C*s,XC*x)_(P(s,XftColorAllocName(dp,vi,cm,s,x))P(i<LEN(cnm)&&cnm[i],xlc(0,cnm[i],x))
 UH r,g,b;i-=16;i<216?r=d6(i/36),g=d6(i/6),b=d6(i):(r=g=b=0x0808+0x0a0a*(i-216));XftColorAllocValue(dp,vi,cm,&(XRenderColor){.red=r,.green=g,.blue=b,.alpha=0xffff},x))
S V xlC(){S I l;I(l,i(nclr,XftColorFree(dp,vi,cm,clrs+i)))E(nclr=MX(LEN(cnm),256);Q(nclr<=LEN(clrs)))i(nclr,Q(xlc(i,0,clrs+i)))l=1;}
S I xgc(I x,UC*r,UC*g,UC*b)_(P(x>(UI)nclr,1)*r=clrs[x].color.red>>8;*g=clrs[x].color.green>>8;*b=clrs[x].color.blue>>8;0)
S I xcn(I x,O C*s)_(P(x>(UI)nclr,1)XC c;P(!xlc(x,s,&c),1)XftColorFree(dp,vi,cm,clrs+x);clrs[x]=c;0)
S I xic0(XIC x,XPointer y,XPointer z)_(xic=0;1)
S V xim0(XIM,XPointer,XPointer);
S I ximopen(Display*d)_(XIMCallback c={.callback=xim0};XICCallback c1={.callback=xic0};xim=XOpenIM(d,0,0,0);P(!xim,0)I(XSetIMValues(xim,XNDestroyCallback,&c,0),PE("could not set XNDestroyCallback\n"))
 spots=XVaCreateNestedList(0,XNSpotLocation,&spot,0);I(!xic,xic=XCreateIC(xim,XNInputStyle,XIMPreeditNothing|XIMStatusNothing,XNClientWindow,xw,XNDestroyCallback,&c1,0))QW(xic);1)
S V xim1(Display*d,XPointer y,XPointer z){I(ximopen(d),XUnregisterIMInstantiateCallback(dp,0,0,0,xim1,0))}
S V xim0(XIM x,XPointer y,XPointer z){xim=0;XRegisterIMInstantiateCallback(dp,0,0,0,xim1,0);XFree(spots);}
S V xpc(XColor*c,I i,I d){I(!XParseColor(dp,cm,cnm[i],c),c->red=c->green=c->blue=d)}
S V xinit(){dp=Q(XOpenDisplay(0));xws=XDefaultScreen(dp);vi=XDefaultVisual(dp,xws);Q(FcInit());uf=FNT;xlF(uf,0);cm=XDefaultColormap(dp,xws);xlC();wi.w=2*bp+tw*wi.cw;wi.h=2*bp+th*wi.ch;
 I(xwg&XNegative,xwx+=DisplayWidth(dp,xws)-wi.w-2)I(xwg&YNegative,xwy+=DisplayHeight(dp,xws)-wi.h-2)ats.background_pixel=clrs[db].pixel;ats.border_pixel=clrs[db].pixel;ats.colormap=cm;
 ats.bit_gravity=NorthWestGravity;ats.event_mask=FocusChangeMask|KeyPressMask|KeyReleaseMask|ExposureMask|VisibilityChangeMask|StructureNotifyMask|ButtonMotionMask|ButtonPressMask|ButtonReleaseMask;
 aE=xi("_XEMBED");aD=xi("WM_DELETE_WINDOW");aN=xi("_NET_WM_NAME");aI=xi("_NET_WM_ICON_NAME");aU=xi("UTF8_STRING");aC=xi("CLIPBOARD");aX=xi("INCR");aT=xi("TARGETS");
 Window par=XRootWindow(dp,xws);xw=XCreateWindow(dp,par,xwx,xwy,wi.w,wi.h,0,XDefaultDepth(dp,xws),InputOutput,vi,CWBackPixel|CWBorderPixel|CWBitGravity|CWEventMask|CWColormap,&ats);
 XGCValues v;memset(&v,0,SZ v);v.graphics_exposures=0;gc=XCreateGC(dp,par,GCGraphicsExposures,&v);dwb=XCreatePixmap(dp,xw,wi.w,wi.h,DefaultDepth(dp,xws));
 XSetForeground(dp,gc,clrs[db].pixel);XFillRectangle(dp,dwb,gc,0,0,wi.w,wi.h);drw=XftDrawCreate(dp,dwb,vi,cm);I(!ximopen(dp),XRegisterIMInstantiateCallback(dp,0,0,0,xim1,0))
 Cursor c=XCreateFontCursor(dp,XC_xterm);XDefineCursor(dp,xw,c);XColor xfg,xbg;xpc(&xfg,7,-1);xpc(&xbg,0,0);XRecolorCursor(dp,c,&xfg,&xbg);XSetWMProtocols(dp,xw,&aD,1);
 XChangeProperty(dp,xw,xi("_NET_WM_PID"),XA_CARDINAL,32,PropModeReplace,(V*)(pid_t[]){getpid()},1);wi.m=WN;xst(0);xhs();XMapWindow(dp,xw);XSync(dp,0);cgt(&tc0);tc1=tc0;pri=cbd=0;I(!aU,aU=XA_STRING)}
S I xmG(GFS*s,O G*g,I n,I x,I y)_(I wx=bp+x*wi.cw,wy=bp+y*wi.ch,px,py;UH m,pm=USHRT_MAX;Fnt*f=fnts;I fl=0,w=wi.cw;FcPattern*p,*q;FcCharSet*fccs;I ns=0;px=wx;py=wy+f->asc;
 i(n,UI u=g[i].u;m=g[i].a;I(m==AD,continue)I(pm-m,pm=m;w=wi.cw*(m&AWi?2:1);fl=!!(m&AB)<<1|!!(m&AI);f=fnts+fl;py=wy+f->asc)
  I k=XftCharIndex(dp,f->m,u);I(k,s[ns].font=f->m;s[ns].glyph=k;s[ns].x=px;s[ns].y=py;px+=w;ns++;continue)I j=0;W(j<nfc,k=XftCharIndex(dp,fc[j].f,u);I(fc[j].m==fl&&(k||fc[j].u==u),BR)j++)
  I(j>=nfc,FcResult r;I(!f->set,f->set=FcFontSort(0,f->p,1,0,&r))p=FcPatternDuplicate(f->p);fccs=FcCharSetCreate();FcCharSetAddChar(fccs,u);FcPatternAddCharSet(p,FC_CHARSET,fccs);
   FcPatternAddBool(p,FC_SCALABLE,1);FcConfigSubstitute(0,p,FcMatchPattern);FcDefaultSubstitute(p);q=FcFontSetMatch(0,(FcFontSet*[]){f->set},1,p,&r);Q(nfc<LEN(fc));
   fc[nfc].f=Q(XftFontOpenPattern(dp,q));fc[nfc].m=fl;fc[nfc].u=u;k=XftCharIndex(dp,fc[nfc].f,u);j=nfc++;FcPatternDestroy(p);FcCharSetDestroy(fccs))
  s[ns].font=fc[j].f;s[ns].glyph=k;s[ns].x=px;s[ns].y=py;px+=w;ns++)ns)
S V xdG(O GFS*s,G g,I n,I x,I y){I wx=bp+x*wi.cw,wy=bp+y*wi.ch,wid=wi.cw*n*(g.a&AWi?2:1);XC*f,*b,rf,rb,tf,tb;UI da=11;//_f:foreground,_b:background,da:default attr
 I(g.a&AI&&g.a&AB,I(fnts[3].badsl||fnts[3].badwt,g.f=da))J((g.a&AI&&fnts[1].badsl)||(g.a&AB&&fnts[2].badwt),g.f=da)
 XRenderColor cf;I(g.f>>24,cf.alpha=0xffff;cf.red=g.f>>8&0xff00;cf.green=g.f&0xff00;cf.blue=g.f<<8&0xff00;XftColorAllocValue(dp,vi,cm,&cf,&tf);f=&tf)E(f=clrs+g.f)
 XRenderColor cb;I(g.b>>24,cb.alpha=0xffff;cb.red=g.b>>8&0xff00;cb.green=g.b&0xff00;cb.blue=g.b<<8&0xff00;XftColorAllocValue(dp,vi,cm,&cb,&tb);b=&tb)E(b=clrs+g.b)
 I((g.a&(AB|AF))==AB&&g.f<8u,f=clrs+g.f+8)
 I(wM(WR),I(f==clrs+df,f=clrs+db)E(cf.red=~f->color.red;cf.green=~f->color.green;cf.blue=~f->color.blue;cf.alpha=f->color.alpha;XftColorAllocValue(dp,vi,cm,&cf,&rf);f=&rf)
          I(b==clrs+db,b=clrs+df)E(cb.red=~b->color.red;cb.green=~b->color.green;cb.blue=~b->color.blue;cb.alpha=b->color.alpha;XftColorAllocValue(dp,vi,cm,&cb,&rb);b=&rb))
 I((g.a&(AB|AF))==AF,             cf.red=f->color.red/2;cf.green=f->color.green/2;cf.blue=f->color.blue/2;cf.alpha=f->color.alpha;XftColorAllocValue(dp,vi,cm,&cf,&rf);f=&rf)
 I(g.a&AR,SWP(f,b))I(g.a&ABl&&wi.m&WB,f=b)I(g.a&AIn,f=b)I(!x,xcl(0,y?wy:0,bp,wy+wi.ch+(wy+wi.ch>=bp+wi.th?wi.h:0)))I(wx+wid>=bp+wi.tw,xcl(wx+wid,y?wy:0,wi.w,wy+wi.ch>=bp+wi.th?wi.h:wy+wi.ch))
 I(!y,xcl(wx,0,wx+wid,bp))I(wy+wi.ch>=bp+wi.th,xcl(wx,wy+wi.ch,wx+wid,wi.h))XR(b,wx,wy,wid,wi.ch);XRectangle r={.width=wid,.height=wi.ch};XftDrawSetClipRectangles(drw,wx,wy,&r,1);
 XftDrawGlyphFontSpec(drw,f,s,n);I(g.a&AU,XR(f,wx,wy+fnts[0].asc+1,wid,1))I(g.a&AS,XR(f,wx,wy+2*fnts[0].asc/3,wid,1))XftDrawSetClip(drw,0);}
S V xdg(G g,I x,I y){GFS s;xdG(&s,g,xmG(&s,&g,1,x,y),x,y);}
S V xdc(I x,I y,G g,I px,I py,G pg){XC c;I(sel(px,py),pg.a^=AR)xdg(pg,px,py);P(wM(WH))g.a&=AB|AI|AU|AS|AWi;
 I s=sel(x,y);I(wM(WR),g.a|=AR;g.b=df;g.f=dcs+s;c=clrs[dcs+!s])E(g.b=dcs+s;g.f=df+!s;c=clrs[g.b])
 I(wM(WF),I t=2,i=wi.cur;i<3u?xdg(g,x,y):i<5u?XR(&c,bp+x*wi.cw,bp+(y+1)*wi.ch-t,wi.cw,t):i<7u?XR(&c,bp+x*wi.cw,bp+y*wi.ch,t,wi.ch):0)
 E(I cw=wi.cw,ch=wi.ch,x0=bp+x*cw,y0=bp+y*ch;XR(&c,x0,y0,cw,1);XR(&c,x0,y0,1,ch);XR(&c,x0+cw-1,y0,1,ch);XR(&c,x0,y0+ch-1,cw,1))}
S V xs_(C*s,I i){s=s?s:"ngn/st";XTextProperty p;P(Xutf8TextListToTextProperty(dp,&s,1,XUTF8StringStyle,&p))(i?XSetWMIconName:XSetWMName)(dp,xw,&p);XSetTextProperty(dp,xw,&p,i?aI:aN);XFree(p.value);}
S V xsi(C*s){xs_(s,1);}S V xst(C*s){xs_(s,0);}
S V xdl(G*l,I x0,I y0,I x1){G g0;GFS*f=gfs;I n=xmG(f,l+x0,x1-x0,x0,y0),i=0,o=0;
 for(I x=x0;x<x1&&i<n;x++){G g=l[x];I(g.a==AD,continue)I(sel(x,y0),g.a^=AR)I(i>0&&(g0.a-g.a||g0.f-g.f||g0.b-g.b),xdG(f,g0,i,o,y0);f+=i;n-=i;i=0)I(!i,o=x;g0=g)i++;} I(i>0,xdG(f,g0,i,o,y0))}
S I xd1()_(wM(WV))S V xd0(){XCopyArea(dp,dwb,xw,gc,0,0,wi.w,wi.h,0,0);XSetForeground(dp,gc,clrs[wM(WR)?df:db].pixel);}
S V xxs(I x,I y){P(xic)spot.x=bp+x*wi.cw;spot.y=bp+(y+1)*wi.ch;XSetICValues(xic,XNPreeditAttributes,spots,0);}
S V xpm(I s){BT(ats.event_mask,s,PointerMotionMask);XChangeWindowAttributes(dp,xw,CWEventMask,&ats);}
S V xm(I s,UI x){I m=wi.m;BT(wi.m,s,x);I((wi.m^m)&WR,rdr())}
S I xsc(I x)_(x<8u?wi.cur=x,0:1)
S V xsu(I x){V(h,XGetWMHints(dp,xw))BT(h->flags,x,XUrgencyHint);XSetWMHints(dp,xw,h);XFree(h);}
S V xbe(){I(!wM(WF),xsu(1))I(VOL>-100,XkbBell(dp,xw,VOL,0))}
S C*kmap(KeySym s,UI x)_(P((UH)s<0xfd00,0)p(key,P(p->k==s&&mtc(p->m,x)&&!(wM(WAK)?p->ak<0:p->ak>0)&&!(wM(WN)&&p->ak==2)&&!(wM(WAC)?p->ac<0:p->ac>0),p->s))0)

//X EVENT HANDLERS   mrep:mouse report, mact:mouse action
TD XEvent E;S I ex(E*e)_(I x=e->xbutton.x-bp;LM(x,wi.tw)x/wi.cw)S I ey(E*e)_(I y=e->xbutton.y-bp;LM(y,wi.th)y/wi.ch)S UI bmsk(UI x)_(x?Button1Mask<<x-1:0)
S V mrep(E*e){I b,c,x=ex(e),y=ey(e),a=e->xbutton.state;S I px,py;
 I(e->type==MotionNotify,P(x==px&&y==py)P(!wM(WMMo)&&!wM(WMMa))P(wM(WMMo)&&!btns)b=__builtin_ctz(1<<11|btns)+1;c=32)E(b=e->xbutton.button;P(b-1>10u)I(e->type==ButtonRelease,P(wM(WX)||b-4<2u))c=0)
 px=x;py=y;c+=(!wM(WMG)&&e->type==ButtonRelease)||b==12?3:b>7?b+120:b>3?b+60:b-1;I(!wM(WX),c+=(a&ShiftMask?4:0)+(a&Mod4Mask?8:0)+(a&ControlMask?16:0))
 C s[40];I n;I(wM(WMG),n=snprintf(s,SZ s,"\e[<%d;%d;%d%c",c,x+1,y+1,"Mm"[e->type==ButtonRelease]);ttyw(s,n,0))J(x<223&&y<223,n=snprintf(s,SZ s,"\e[M%c%c%c",c+32,x+33,y+33);ttyw(s,n,0))}
S I mact(E*e,UI r)_(UI b=e->xbutton.button,x=e->xbutton.state&~bmsk(b);p(mshc,P(p->release==r&&p->btn==b&&(!p->altscrn||p->altscrn==(talt()?1:-1))&&(mtc(p->m,x)||mtc(p->m,x&~FM)),p->f();1))0)
S V msel(E*e,I done){selx(ex(e),ey(e),1,done);P(!done)C*s=gsel();P(!s)free(pri);pri=s;XSetSelectionOwner(dp,XA_PRIMARY,xw,e->xbutton.time);I(XGetSelectionOwner(dp,XA_PRIMARY)-xw,selcl())}
#define h(f,a...) S V f(E*x){a;}
h(ek,P(wM(WK))V(e,&x->xkey)C s[64];KeySym ks;Status u;I n=xic?XmbLookupString(xic,e,s,SZ s,&ks,&u):XLookupString(e,s,SZ s,&ks,0);p(shc,P(ks==p->k&&mtc(p->m,e->state),p->f();))
 C*k=kmap(ks,e->state);P(k,ttyw(k,strlen(k),1);)P(!n)I(n==1&&e->state&_1,I(wM(W8),I(*s<127,UI c=*s|128;n=u8e(c,s)))E(s[1]=*s;*s=27;n=2))ttyw(s,n,1))
h(eM,V(c,&x->xclient)I(c->message_type==aE&&c->format==32,I l=c->data.l[1];l==4?wi.m|=WF,xsu(0):l==5?wi.m&=~WF:0)J(c->data.l[0]==aD,ttyhup();exit(0)))
h(ec,V(c,&x->xconfigure)V(w,c->width)V(h,c->height)P(w==wi.w&&h==wi.h)csz(w,h))h(ev,BT(wi.m,x->xvisibility.state-VisibilityFullyObscured,WV))h(eu,wi.m&=~WV)h(ee,rdr())
h(ef,P(x->xfocus.mode==NotifyGrab)I(x->type==FocusIn,I(xic,XSetICFocus(xic))wi.m|=WF;xsu(0);I(wM(Wf),ttyw("\e[I",3,0)))E(I(xic,XUnsetICFocus(xic))wi.m&=~WF;I(wM(Wf),ttyw("\e[O",3,0))))
h(em,wM(WMM)&&!(x->xbutton.state&FM)?mrep(x):msel(x,0))
h(eb,I b=x->xbutton.button;btns|=(b-1<10u)<<b-1;P(wM(WMM)&&!(x->xbutton.state&FM),mrep(x))P(mact(x,0))
 I(b==1,ST timespec t;cgt(&t);I sn=TDF(t,tc1)<600?2:TDF(t,tc0)<300?1:0;tc1=tc0;tc0=t;selst(ex(x),ey(x),sn)))
h(eB,I b=x->xbutton.button;btns&=~((b-1<10u)<<b-1);P(wM(WMM)&&!(x->xbutton.state&FM),mrep(x))P(mact(x,1))I(b==1,msel(x,1)))
h(es,unsigned long r=1,o=0;Atom a=x->type==SelectionNotify?x->xselection.property:x->type==PropertyNotify?x->xproperty.atom:0;P(!a)
 W(r,I fmt;Atom t;unsigned long n;UC*d;P(XGetWindowProperty(dp,xw,a,o,2048,0,AnyPropertyType,&t,&fmt,&n,&r,&d),PE("clipboard allocation failed\n");{})
  I(x->type==PropertyNotify&&!n&&!r,BT(ats.event_mask,0,PropertyChangeMask);XChangeWindowAttributes(dp,xw,CWEventMask,&ats))
  I(t==aX,BT(ats.event_mask,1,PropertyChangeMask);XChangeWindowAttributes(dp,xw,CWEventMask,&ats);XDeleteProperty(dp,xw,a);continue)
  UC*r=d,*l=d+n*fmt/8;W((r=memchr(r,10,l-r)),*r++=13)I(wM(WBP)&&!o,ttyw("\e[200~",6,0))ttyw(d,n*fmt/8,1);I(wM(WBP)&&!r,ttyw("\e[201~",6,0))XFree(d);o+=n*fmt/32)
 XDeleteProperty(dp,xw,a))
h(ep,V(e,&x->xproperty)I(e->state==PropertyNewValue&&(e->atom==XA_PRIMARY||e->atom==aC),es(x)))
h(eS,XSelectionRequestEvent*y=(V*)x;XSelectionEvent z={.type=SelectionNotify,.requestor=y->requestor,.selection=y->selection,.target=y->target,.time=y->time};DF(y->property,y->target);
 I(y->target==aT,XChangeProperty(y->display,y->requestor,y->property,XA_ATOM,32,PropModeReplace,(UC*)&aU,1);z.property=y->property)
 J(y->target==aU||y->target==XA_STRING,C*s=y->selection==XA_PRIMARY?pri:y->selection==aC?cbd:0;P(!s,PE("unhandled clipboard selection:0x%lx\n",y->selection);{})
  I(s,XChangeProperty(y->display,y->requestor,y->property,y->target,8,PropModeReplace,(V*)s,strlen(s));z.property=y->property))
 QW(XSendEvent(y->display,y->requestor,1,0,(V*)&z)))
S TY(ek)*eh[LASTEvent]={[KeyPress]=ek,[ClientMessage]=eM,[ConfigureNotify]=ec,[VisibilityNotify]=ev,[UnmapNotify]=eu,[Expose]=ee,[FocusIn]=ef,[FocusOut]=ef,[MotionNotify]=em,[ButtonPress]=eb,
 [ButtonRelease]=eB,[SelectionNotify]=es,[PropertyNotify]=ep,[SelectionRequest]=eS};

I main()_(xsc(2);setlocale(LC_CTYPE,"");XSetLocaleModifiers("");tnew(80,24);xinit();C s[32];snprintf(s,SZ s,"%lu",xw);setenv("WINDOWID",s,1);
 E x;I w=wi.w,h=wi.h;W(1,XNextEvent(dp,&x);I(!XFilterEvent(&x,0)&&x.type==ConfigureNotify,w=x.xconfigure.width;h=x.xconfigure.height)I(x.type==MapNotify,BR))
 I xfd=XConnectionNumber(dp),dwg=0/*drawing?*/;ttynew("/bin/sh");csz(w,h);double to=-1;ST timespec seltv,*tv,t,trgr,lb={}/*last blink*/;
 W(1,fd_set r;FD_ZERO(&r);FD_SET(cfd,&r);FD_SET(xfd,&r);I(XPending(dp),to=0)seltv.tv_sec=to/1E3;seltv.tv_nsec=1E6*(to-1E3*seltv.tv_sec);tv=to>=0?&seltv:0;
  I(pselect(MX(xfd,cfd)+1,&r,0,0,tv,0)<0,Q(errno==EINTR))cgt(&t);I(FD_ISSET(cfd,&r),ttyr())I xev=0;W(XPending(dp),xev=1;XNextEvent(dp,&x);I(XFilterEvent(&x,0),continue)V(f,eh[x.type])I(f,f(&x)))
  I(FD_ISSET(cfd,&r)||xev,I(!dwg,trgr=t;dwg=1)to=(1-TDF(t,trgr)/33)*8;I(to>0,continue))
  to=-1;I b=800/*blink timeout*/;I(b&&tat(ABl),to=b-TDF(t,lb);I(to<1,I(-to>b,wi.m|=WB)wi.m^=WB;tdirtat(ABl);lb=t;to=b))draw();XFlush(dp);dwg=0)0)
